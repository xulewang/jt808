package com.ltmonitor.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 功能权限 用于对角色分配权限
 * @author DELL
 *
 */
@Entity
@Table(name = "functionModel")
@org.hibernate.annotations.Proxy(lazy = false)
public class FuncModel extends TenantEntity implements Serializable {
	private static final long serialVersionUID = -6023569156743874873L;
	public static final Integer LEVEL_GROUT = Integer.valueOf(1);

	public static final Integer LEVEL_ITEM = Integer.valueOf(2);

	public static final Integer FUNC_TYPE_WEB = Integer.valueOf(1);

	public static final Integer FUNC_TYPE_WEB_MONITER = Integer.valueOf(2);
	public static final int CARCONTROL_FUNC_GROUPID = 900000;
	private int  entityId;
	private String name;
	private String url;
	private String descr;
	private Integer level;
	private Integer funcType;
	private Integer parentId;
	//private Integer style;
	
	private String icon;

	@Transient
	private Integer reportFlag;
	public static int REPORT_FLAG_LINK = 0;

	public static int REPORT_FLAG_SELF = 1;

	public static int REPORT_FLAG_TEMPLATE = 2;
	private Integer flag;
	private Integer menuSort;
	private Integer belongTo;
	private Integer fx;
	private Integer mask;

	public FuncModel() {
	}

	public FuncModel(Integer id) {
		this.entityId = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "funcId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}

	@Column(name = "url", length = 50)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "descr")
	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Column(name = "level")
	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@Column(name = "funcType")
	public Integer getFuncType() {
		return this.funcType;
	}

	public void setFuncType(Integer funcType) {
		this.funcType = funcType;
	}

	@Column(name = "name")
	public String getFuncName() {
		return this.name;
	}

	public void setFuncName(String funcName) {
		this.name = funcName;
	}

	@Column(name = "parentId")
	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Column(name = "flag")
	public Integer getFlag() {
		return this.flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Transient
	public Integer getReportFlag() {
		if (this.reportFlag == null) {
			return Integer.valueOf(0);
		}
		return this.reportFlag;
	}

	public void setReportFlag(Integer reportFlag) {
		this.reportFlag = reportFlag;
	}

	@Column(name = "menuSort")
	public Integer getMenuSort() {
		return this.menuSort;
	}

	public void setMenuSort(Integer menuSort) {
		this.menuSort = menuSort;
	}

	@Column(name = "mask")
	public Integer getMask() {
		if (this.mask == null) {
			return Integer.valueOf(0);
		}
		return this.mask;
	}

	public void setMask(Integer mask) {
		this.mask = mask;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
