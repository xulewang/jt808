package com.ltmonitor.service.impl;

import java.util.List;

import com.ltmonitor.dao.impl.DaoHibernateImpl;
import com.ltmonitor.entity.Enclosure;
import com.ltmonitor.entity.LineBufferPoint;
import com.ltmonitor.entity.LineSegment;
import com.ltmonitor.service.IEnclosureService;

public class EnclosureService extends DaoHibernateImpl implements
		IEnclosureService {

	public void saveRoute(Enclosure ec, List<LineSegment> segments) {
		this.saveOrUpdate(ec);
		for (LineSegment ls : segments) {
			ls.setEnclosureId(ec.getEntityId());
		}
		this.saveOrUpdateAll(segments);
		/**
		int m = 0;
		for (LineBufferPoint b : bufferPoints) {
			LineSegment seg = segments.get(b.getNodeNo());
			b.setEnclosureId(seg.getEntityId());
		}
		this.saveOrUpdateAll(bufferPoints);
		*/
	}

	public List<LineSegment> getLineSegments(int enclosureId) {
		String hql = "from LineSegment where enclosureId = ? and deleted = ?";
		List<LineSegment> result = (List) this.query(hql, new Object[] {
				enclosureId, false });

		return result;
	}

}
