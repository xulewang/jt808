package com.ltmonitor.jt808.service.impl;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.ltmonitor.app.GlobalConfig;
import com.ltmonitor.app.GpsConnection;
import com.ltmonitor.entity.TerminalCommand;
import com.ltmonitor.jt808.protocol.T808Message;
import com.ltmonitor.jt808.service.IAckService;
import com.ltmonitor.jt808.service.ICommandHandler;
import com.ltmonitor.jt808.service.ICommandService;
import com.ltmonitor.jt808.service.IJT808Server;
import com.ltmonitor.jt808.service.IMessageSender;
import com.ltmonitor.jt808.service.IT808Manager;
import com.ltmonitor.jt808.service.ITransferGpsService;


/**
 * 提供外部调用808转发平台的接口
 * 
 * @author DELL
 * 
 */
public class T808Manager implements IT808Manager {

	private static Logger logger = Logger.getLogger(T808Manager.class);
	
	private int listenPort;
	

	private IJT808Server jt808Server;
	
	//private static ConcurrentLinkedQueue<T808Message> dataQueue = new ConcurrentLinkedQueue();

	// 流水号
	private static int serialNo = 0;

	private ICommandService commandService;

	//private Thread processRealDataThread;
	
	private IAckService ackService;
	

	// 数据转发服务,主要用于809转发
	private ITransferGpsService transferGpsService;

	
	public static int getSerialNo() {
		return serialNo++;
	}

	public Collection<GpsConnection> getGpsConnections() {
		return getJt808Server().getGpsConnections();
	}

	// 向上级平台发送数据
	private  boolean send(String simNo, byte[] msg) {
		// 优先使用主链路发送数据
		boolean res = getJt808Server().send(simNo, msg);
		return res;
	}

	// 向发送数据
	public boolean Send(T808Message msg) {
		msg.getHeader().setMessageSerialNo((short) getSerialNo());
		// msg.setPacketDescr(strMsg);
		boolean res = send(msg.getSimNo(), msg.WriteToBytes());
		GlobalConfig.putMsg(msg);
		return res;
	}
	
	
	public boolean StartServer() {
		// 启动服务器
		boolean res = getJt808Server().start();
		if (res) {
			// 启动命令解析器，从数据库中读取命令进行解析
			//getCommandService().setBaseDao(ServiceLauncher.getBaseDao());
			getCommandService().Start();
			
		    this.commandService.setOnRecvCommand(new ICommandHandler(){

				@Override
				public boolean OnRecvCommand(T808Message tm, TerminalCommand tc) {
					if(getJt808Server().isOnline(tc.getSimNo()) == false)
					{
						tc.setStatus(TerminalCommand.STATUS_OFFLINE);
						return false;
					}else
					{
						boolean res = Send(tm);
						tc.setSN(tm.getHeader().getMessageSerialNo());
						tc.setStatus(res ? TerminalCommand.STATUS_PROCESSING : 
							TerminalCommand.STATUS_FAILED);
						return res;
					}				
				}
		    });
			
			
			//应答服务，调用Server，发送应答数据包
			ackService.setMessageSender(new IMessageSender(){

				@Override
				public void Send808Message(T808Message tm) {
					Send(tm);
					
				}
				
			});
			//if(transferGpsService.sta)
			transferGpsService.start();
			//processRealDataThread.start();
		}
		return res;
	}
	


	/**
	 * 停止服务
	 */
	public void StopServer() {
		//DNSSTimer.getInstance().Stop();
		getJt808Server().Stop();
		transferGpsService.stop();

	}

	
	public void setJt808Server(IJT808Server jt808Server) {
		this.jt808Server = jt808Server;
	}

	public IJT808Server getJt808Server() {
		return jt808Server;
	}

	public void setCommandService(ICommandService commandService) {
		this.commandService = commandService;
	}

	public ICommandService getCommandService() {
		return commandService;
	}

	public void setAckService(IAckService ackService) {
		this.ackService = ackService;
	}

	public IAckService getAckService() {
		return ackService;
	}

	public int getListenPort() {
		return listenPort;
	}

	public void setListenPort(int listenPort) {
		this.listenPort = listenPort;
	}

	public ITransferGpsService getTransferGpsService() {
		return transferGpsService;
	}

	public void setTransferGpsService(ITransferGpsService transferGpsService) {
		this.transferGpsService = transferGpsService;
	}

}
